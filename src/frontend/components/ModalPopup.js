import React from 'react';

const ModalPopup = props => {
    return (
        <div className={`modal fade ${props.open && 'show'}`} tabIndex="-1" role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3 className="modal-title">{props.title}</h3>
                        <button type="button" className="close" aria-label="Close" onClick={() => props.handleClose()}>
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="card p-2 bg-light">
                            <h5 className="text-center"> Manufacturer</h5>
                            <div className="row no-gutters">
                                <ul className="col-sm-5 text-right list-none">
                                    <li>Name :</li>
                                    <li>Date :</li>
                                    <li>Location :</li>
                                </ul>
                                <ul className="col-sm-6 list-none">
                                    <li>Abbott Laboratories</li>
                                    <li>2018/09/19</li>
                                    <li>
                                        <address>
                                            <strong>Abbott Laboratories.</strong><br/>
                                            Chicago, Illinois<br/>
                                            United States<br/>
                                        </address>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="row justify-content-center no-gutters mt-1 mb-1 f30">
                            <i className="fa fa-chevron-circle-down"></i>
                        </div>
                        <div className="card p-2 bg-light">
                            <h5 className="text-center"> Distributor</h5>
                            <div className="row no-gutters">
                                <ul className="col-sm-5 text-right list-none">
                                    <li>Name :</li>
                                    <li>Date :</li>
                                    <li>Location :</li>
                                </ul>
                                <ul className="col-sm-6 list-none">
                                    <li>Roslin Pharmaceuticals</li>
                                    <li>2018/09/19</li>
                                    <li>
                                        <address>
                                            <strong>Abbott Laboratories.</strong><br/>
                                            Chicago, Illinois<br/>
                                            United States<br/>
                                        </address>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => props.handleClose()}>Close</button>
                        <button type="button" className="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalPopup;
