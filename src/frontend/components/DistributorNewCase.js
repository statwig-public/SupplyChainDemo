import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as services from '../../blockchain/library/pharmalib';

import { storeDistributorTransaction, storeCaseID } from '../actions';
class DistributorNewCase extends Component {
  state = {
    caseIdPharma: "GTIN "+this.generateCaseIds(),
    caseIdHospital: "GTIN "+ this.generateCaseIds(),
    selectedCaseIdsPharma: [],
    selectedCaseIdsHospital: [],
    pharmaCaseIds: [],
    hospitalCaseIds: [],
  };

  componentWillMount() {
    const { transactions, multiChainTransactions } = this.props;
    const caseIdsArray =
      transactions.length > 0
        ? transactions[0].bottleList.split(',')
        : [];
    const caseIdsArrayMC =
      multiChainTransactions.length > 0
        ? multiChainTransactions[0].bottleList.split(',')
        : [];
    const totalCaseIds = [...caseIdsArray, ...caseIdsArrayMC];
    const caseIds = totalCaseIds.map(caseId => ({ active: false, id: caseId }));
    this.setState({ pharmaCaseIds: caseIds, hospitalCaseIds: caseIds });
  }

  generateCaseIds() {
    return (
      Math.floor(100000 + Math.random() * 900000) +
      '-' +
      Math.floor(100000 + Math.random() * 900000) +
      '-' +
      Math.floor(1000 + Math.random() * 9000)
    );
  }


  async makeCase(caller, location, bottleList) {
    var result = '';
    if (caller === 'manufacturer') {
      result = await services.makeManufacturerCase(
        this.state.caseId,
        location,
        bottleList,
      );
    } else {
      result = await services.makeDistributorCase(
        this.state.pharmaID,
        location,
        bottleList,
      );
      result = await services.makeDistributorCase(
        this.state.hospID,
        location,
        bottleList,
      );
    }
    this.setState({ txnHash: result.txnHash });
    this.props.storeTransaction(result.txnHash);
    this.props.history.push('/');
  }

  onCaseIdSelectLeftPharma(index) {
    let caseIds = this.state.pharmaCaseIds;
    caseIds = JSON.parse(JSON.stringify(caseIds));
    caseIds[index].active = !caseIds[index].active;
    this.setState({ pharmaCaseIds: caseIds });
  }

  onCaseIdSelectLeftHospital(index) {
    let caseIds = this.state.hospitalCaseIds;
    caseIds = JSON.parse(JSON.stringify(caseIds));
    caseIds[index].active = !caseIds[index].active;
    this.setState({ hospitalCaseIds: caseIds });
  }

  onCaseIdSelectRightPharma(index) {
    let selectedCaseIds = this.state.selectedCaseIdsPharma;
    selectedCaseIds = JSON.parse(JSON.stringify(selectedCaseIds));
    selectedCaseIds[index].active = !selectedCaseIds[index].active;
    this.setState({ selectedCaseIdsPharma: selectedCaseIds });
  }
  onCaseIdSelectRightHospital(index) {
    let selectedCaseIds = this.state.selectedCaseIdsHospital;
    selectedCaseIds = JSON.parse(JSON.stringify(selectedCaseIds));
    selectedCaseIds[index].active = !selectedCaseIds[index].active;
    this.setState({ selectedCaseIdsHospital: selectedCaseIds });
  }

  onSubmit = async () => {

    const { selectedCaseIdsPharma, caseIdPharma, selectedCaseIdsHospital, caseIdHospital } = this.state;

    this.props.storeCaseID({caseIdPharma, caseIdHospital});
    const bottleListPharma = selectedCaseIdsPharma.reduce(
      (init, { id }, index) => {
        if (index === 0) return id;
        else return init + ',' + id;
      },
      '',
    );
    const bottleListHospital = selectedCaseIdsHospital.reduce(
      (init, { id }, index) => {
        if (index === 0) return id;
        else return init + ',' + id;
      },
      '',
    );
   const result = await services.makeDistributorCase(
      caseIdPharma,
      'SanJose',
      bottleListPharma,
      async (res) => {
        let transactionObject = {
          transactionHash: res,
          bottleListPharma,
          caseIdPharma,
        };
        this.props.storeDistributorTransaction(transactionObject);
        transactionObject = {
          bottleListHospital,
          caseIdHospital,
        }
        this.props.storeDistributorTransaction(transactionObject);
        this.props.history.push('/');
      }
    );
   console.log('resultdistributor', result);
    await services.makeDistributorCase(
      caseIdHospital,
      'SanJose',
      bottleListHospital,
      (transactionHash) => {
        const transactionObject = {
          transactionHash,
          bottleListHospital,
          caseIdHospital,
        };
        this.props.storeDistributorTransaction(transactionObject);
      }
    );
  };

  onRightArrowClickPharma = () => {
    const selectedCaseIds = this.state.pharmaCaseIds.filter(
      caseId => caseId.active,
    );
    const pharmaCaseIds = this.state.pharmaCaseIds.filter(
      caseId => !caseId.active,
    );
    selectedCaseIds.forEach(caseId => (caseId.active = false));
    this.setState({
      selectedCaseIdsPharma: [
        ...this.state.selectedCaseIdsPharma,
        ...selectedCaseIds,
      ],
      pharmaCaseIds,
      hospitalCaseIds: pharmaCaseIds,
    });
  };

  onRightArrowClickHospital = () => {
    const selectedCaseIds = this.state.hospitalCaseIds.filter(
      caseId => caseId.active,
    );
    const hospitalCaseIds = this.state.hospitalCaseIds.filter(
      caseId => !caseId.active,
    );
    selectedCaseIds.forEach(caseId => (caseId.active = false));
    this.setState({
      selectedCaseIdsHospital: [
        ...this.state.selectedCaseIdsHospital,
        ...selectedCaseIds,
      ],
      pharmaCaseIds: hospitalCaseIds,
      hospitalCaseIds,
    });
  };

  onLeftArrowClickPharma = () => {
    const caseIds = this.state.selectedCaseIdsPharma.filter(
      caseId => caseId.active,
    );
    const selectedCaseIdsPharma = this.state.selectedCaseIdsPharma.filter(
      caseId => !caseId.active,
    );
    caseIds.forEach(caseId => (caseId.active = false));
    const pharmaCaseIds = [...this.state.pharmaCaseIds, ...caseIds];
    this.setState({
      pharmaCaseIds,
      hospitalCaseIds: pharmaCaseIds,
      selectedCaseIdsPharma,
    });
  };

  onLeftArrowClickHospital = () => {
    const caseIds = this.state.selectedCaseIdsHospital.filter(
      caseId => caseId.active,
    );
    const selectedCaseIdsHospital = this.state.selectedCaseIdsHospital.filter(
      caseId => !caseId.active,
    );
    caseIds.forEach(caseId => (caseId.active = false));
    const hospitalCaseIds = [...this.state.hospitalCaseIds, ...caseIds];
    this.setState({
      pharmaCaseIds: hospitalCaseIds,
      hospitalCaseIds,
      selectedCaseIdsHospital,
    });
  };
  render() {
    return (
      <div className="container-fluid mt-3">
        <h2>Pharmacy</h2>
        <div className="row">
          <div className="col-sm-6">
            <ul className="list-none">
              <li>
                <b>Product ID:</b> {this.state.caseIdPharma}
              </li>
              <li>
                <b>Batch</b> #BTST12345
              </li>
              <li>
                <b>Expiry Date</b> 12-12-1212
              </li>
              <li>
                <b>Market Authorization Holder:</b> BTST Pharm
              </li>
            </ul>
          </div>
          <div className="col-sm-6">
            <div className=" d-flex align-content-center justify-content-center">
              <button
                className="btn btn-dark mr-3"
                onClick={this.onSubmit}
                disabled={
                  this.state.selectedCaseIdsPharma.length === 0 ||
                  this.state.selectedCaseIdsHospital.length === 0
                }
              >
                Submit
              </button>
              <button
                className="btn btn-dark"
                onClick={() => this.props.history.push('/')}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
        <div className="row mt-4 no-gutters">
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.pharmaCaseIds.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectLeftPharma(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
          <div className="col-sm-1 font30 text-center d-flex align-content-center justify-content-center">
            <i
              className="fa fa-arrow-circle-o-left"
              aria-hidden="true"
              onClick={this.onLeftArrowClickPharma}
            />
            <i
              className="fa fa-arrow-circle-o-right"
              aria-hidden="true"
              onClick={this.onRightArrowClickPharma}
            />
          </div>
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.selectedCaseIdsPharma.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectRightPharma(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <h2>Hospital</h2>

        <div className="row">
          <div className="col-sm-6">
            <ul className="list-none">
              <li>
                <b>Product ID:</b> {this.state.caseIdHospital}
              </li>
              <li>
                <b>Batch</b> #BTST12345
              </li>
              <li>
                <b>Expiry Date</b> 12-12-2022
              </li>
              <li>
                <b>Market Authorization Holder:</b> BTST Pharm
              </li>
            </ul>
          </div>
        </div>
        <div className="row mt-4 no-gutters">
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.hospitalCaseIds.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectLeftHospital(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
          <div className="col-sm-1 font30 text-center d-flex align-content-center justify-content-center">
            <i
              className="fa fa-arrow-circle-o-left"
              aria-hidden="true"
              onClick={this.onLeftArrowClickHospital}
            />
            <i
              className="fa fa-arrow-circle-o-right"
              aria-hidden="true"
              onClick={this.onRightArrowClickHospital}
            />
          </div>
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.selectedCaseIdsHospital.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectRightHospital(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { transactions: state.transactions, multiChainTransactions: state.multiChainTransactions };
}

export default connect(mapStateToProps, { storeDistributorTransaction, storeCaseID })(
  DistributorNewCase,
);
