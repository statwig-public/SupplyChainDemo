import React, { Component } from 'react';
import { connect } from 'react-redux';
//import {Table, Grid, Button, Form } from 'react-bootstrap';
import { Table, Form, Button, Input, Message } from 'semantic-ui-react';
import * as mservices from '../../multichain/pharmalib';



class Duplicacy extends Component {

    state = {
      ipfsHash:null,
      key:'',
      manufactureripfsHash:'',
      distributoripfsHash:'',
      pharmacyipfsHash:'',
      hospitalipfsHash:'',
      status: '',
      manufacturereventDate: '',
      distributoreventDate:'',
      pharmacyeventDate: '',
      hospitaleventDate: '',
    };

    onClick = async () => {

  }


    onSubmit = async (event) => {
 
    var ipfsData =  mservices.detectIpfsDuplicacy(this.state.key);

    if(ipfsData.dataArgs.data.actor === "manufacturer" || ipfsData.dataArgs.data.actor === "Manufacturer"){
	this.setState({ manufactureripfsHash: ipfsData.data.ipfsHash });
	this.setState({ manufacturereventDate: ipfsData.data.date });
    }

    if(ipfsData.dataArgs.data.actor === "distributor" || ipfsData.dataArgs.data.actor === "Distributor"){
	this.setState({ distributoripfsHash: ipfsData.data.ipfsHash });
	this.setState({ distributoreventDate: ipfsData.data.date });
    }

    if(ipfsData.dataArgs.data.actor === "pharmacy" || ipfsData.dataArgs.data.actor === "Pharmacy"){
	this.setState({ pharmacyipfsHash: ipfsData.data.ipfsHash });
	this.setState({ pharmacyeventDate: ipfsData.data.date });
    }

    if(ipfsData.dataArgs.data.actor === "hospital" || ipfsData.dataArgs.data.actor === "Hospital"){
	this.setState({ hospitalipfsHash: ipfsData.data.ipfsHash });
	this.setState({ hospitaleventDate: ipfsData.data.date });
    }


    if(this.state.manufactureripfsHash === this.state.distributoripfsHash){
	this.setState({ status: 'Valid Product'});
    } else{
	this.setState({ status: 'Duplicate'});
    }


    }; //onSubmit

    render() {
      const thStyle = {
        borderRight: '1px solid grey',
        width: '50%'
      }

      const tdStyle = {
        textOverflow: 'clip'
      }
      
      return (
        <div className="Certificate">

      <div className="w3-row" style={{height: '50px'}}>
        <div className="w3-col s5 w3-xlarge">
          <div className=" w3-left-align logoFont" style={{ paddingLeft: '10px', paddingTop: '5px'}}>
            <span><b>STA</b></span><span style={{color: 'green'}}><b>TWIG</b></span>
          </div>
        </div>
      </div>
          <div style={{height: '90px', backgroundColor: '#0F95D8', textAlign: 'center', color: 'white'}}>
            <h2 style={{marginTop: '10px'}}>Duplicaction Detection with scBlockchain</h2>
            <h4 className="h2Size">Powered by StaTwig</h4>
          </div>

          <div>
            <h3> Enter GTIN for Report </h3>
            <Form onSubmit={this.onSubmit}>


          <Form.Field>
            <Input className="w3-large"
              label="Product GTIN"
              labelPosition="right"
              onChange={event =>
                this.setState({ key: event.target.value })
              }
            />
          </Form.Field>


              <div >
		<div style={{marginLeft: '0px', marginTop: '0px'}}>
                  <button type="submit" className="certificateSumitBtn" >
                    <i className="fa fa-paper-plane-o" aria-hidden="true" style={{ fontSize: '80px' }}></i><br/>
                    GetReport
                  </button >
		</div>
              </div>
              
              
            </Form> 
               
          </div>

<br/>

          <div style={{maxHeight: '450px', backgroundColor: '#0F95D8'}}>
           


            <div className="certificateTableBackground" style={{ borderRadius: '10px',  width: '100%',  maxWidth: '100%',  backgroundColor: 'white'}}>
              <div style={{marginTop: '0px'}} className="w3-hide-small">
        <div id="bodyContainerLarge" className="w3-container">   
          <Table style={{marginTop: '20px'}}>
            <tr style={{ borderColor: 'black' }} className="columnHeader">
              <td style={{ borderColor: 'black' }}><b>GTIN</b></td>
              <td style={{ borderColor: 'black' }}><b>Manufacturer IPFS</b></td>
              <td style={{ borderColor: 'black' }}><b>Distributor IPFS</b></td>
              <td style={{ borderColor: 'black' }}><b>Status</b></td>
            </tr>

            <tr style={{ borderColor: 'black' }} className="columnHeader">
              <td style={{ borderColor: 'black' }}>{this.state.key}</td>
              <td style={{ borderColor: 'black' }}>{"QmXCFzZyTmHfUdvtXaE9KUGt3eANpL3JCfKTxsMRFNokvz"}</td>
              <td style={{ borderColor: 'black' }}>{"QmXCFzZyTmHfUdvtXaE9KUGt3eANpL3JCfKTxsMRFNokvz"}</td>
              <td style={{ borderColor: 'black' }}>{"Valid Document"}</td>
            </tr>

            <tr style={{ borderColor: 'black' }} className="columnHeader">
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
            </tr>


          </Table>



        </div>
              </div>
            </div>
            <br/><br/>
            
          </div>
        </div>
      );
    } 
}


export default Duplicacy;

/*
            <div className="w3-container w3-padding-24">
<br/>              <Button type="button" style={{backgroundColor: '#FFC300', color: 'black'}} onClick = {this.onClick}>Get Duplicacy Report</Button>
            </div>
*/
