import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as services from '../../blockchain/library/pharmalib';
import { storeTransaction } from '../actions';

import shippingInfo from '../assets/images/shipment label.png';

class ShipCasePharmacy extends Component {
    getCurrentCaseID() {
        var len = this.props.transactions.length;
        return len && this.props.transactions[len-1].caseId;
    }

    componentWillMount() {
        this.setState({
            caseID: this.props.caseId.caseIdPharma
        });
    }
    constructor(props) {
        super();
        this.state = {
            isActive: false
        }
    }

    generateCaseIds() {
        return (
            Math.floor(100000 + Math.random() * 900000) +
            '-' +
            Math.floor(100000 + Math.random() * 900000) +
            '-' +
            Math.floor(1000 + Math.random() * 9000)
        );
    }
    //Called by the owner of the case
    async shipCase(caller,shippedTo,destLocation) {
        console.log("Printing here");
        await services.shipCase(
            this.state.caseID,
            caller,
            shippedTo,
            destLocation,
            (transactionHash)=> {
                this.setState({ transactionHash });
                const transactionObject = {
                    transactionHash: transactionHash,
                    caseId: this.state.caseID
                };
                this.props.storeTransaction(transactionObject);
                this.props.history.push('/');
            }
        );
    }

    handleNotify = () => {
        this.setState({ isActive: true })
    }

    render() {
        const {ringBell} = this.props;
        const text = ringBell.distributorRingBell ? 'Notify Distributor' : ringBell.pharmaRingBell ? 'Notify Pharma' : ringBell.hospitalRingBell ? 'Notify Hospital' : ''
        const notifyName = ringBell.distributorRingBell ? 'Joe Distributor' : ringBell.pharmaRingBell ? 'Walmart Pharmacy' : ringBell.hospitalRingBell ? 'County Hospital' : ''

        return (
          <div className="container mt-4">
              <div className="row">
                  <div className="col-6"><h3>Shipping Label <br></br>{this.state.caseID}</h3></div>
                  <div className="col-6">
                      <button className="btn btn-dark mb-3 btn-block" onClick={() => this.handleNotify()}>
                        {text}
                      </button>
                      <button className="btn btn-dark" onClick={() => this.shipCase("manufacturer","distributor","San Francisco")}>
                          Submit to Blockchain</button>
                  </div>
              </div>
              <div className="row mt-3">
                  <img alt='shippingInfo' src={shippingInfo}/>
              </div>
              {
                  this.state.isActive &&  <div className="modal fade show" tabIndex="-1" role="dialog">
                      <div className="modal-dialog" role="document">
                          <div className="modal-content">
                              <div className="modal-header">
                                  <h5 className="modal-title">Messages</h5>
                                  <button type="button" className="close"
                                          data-dismiss="modal"
                                          aria-label="Close" onClick={() => this.setState({isActive: false})}>
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div className="modal-body">
                                  <dl>
                                      <dd>{notifyName}</dd>
                                      <dt>Notification received</dt>
                                  </dl>
                              </div>
                          </div>
                      </div>
                  </div>
              }
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        transactions: state.transactions,
        transactionsDistributor: state.transactionsDistributor,
        ringBell: state.ringBell,
        caseId: state.caseId
    };
}

export default connect(mapStateToProps, { storeTransaction }) (ShipCasePharmacy);
