import React from 'react';
import BellIcon from 'react-bell-icon';

const distributor ="https://statwig.com/pharma/static/media/distributor.png";

const Distributor = (props) => {
  return  <div className="row ml-0 mr-0 grey_border">
    <div className="col-sm-6  pl-5 pr-5 pt-3 pb-2">
      <button
        className="btn btn-dark mb-2 btn-block"
        onClick={() => {
          const eventObj = {
            eventName: 'Receive shipment',
            eventDate: new Date().toLocaleString(),
            owner: 'Distributor',
            data: 'location::Santa Clara',
          };
          props.storeEventDetails(eventObj);
          props.history.push('/receiveNewCase');
          props.showRingBell({ distributorRingBell: false });

        }}
      >
        <i className="fa fa-cart-arrow-down" /> Receive new case
        {props.ringBell.distributorRingBell && (
          <div className="bell_position">
            <BellIcon
              width="40"
              active={true}
              animate={true}
              color="darkorange"
            />
          </div>
        )}
      </button>
      <button
        className="btn btn-dark mb-2 btn-block position-relative"
        onClick={() => {
          if (props.ringBell.caseReturned) {
            const eventObj = {
              eventName: 'Verification failed',
              eventDate: new Date().toLocaleString(),
              owner: 'Hospital',
              data: 'confirmedBy::Distributor,expiryDate::21/09/2018',
            };
            props.storeEventDetails(eventObj);
          } else {
            const eventObj = {
              eventName: 'Verified shipment',
              eventDate: new Date().toLocaleString(),
              owner: 'Distributor',
              data: 'approvedBy::Manufacturer',
            };

            props.storeEventDetails(eventObj);
          }
          props.history.push('/verifyCases');

        }}
      >
        <i className="fa fa-handshake-o">Verification</i>
      </button>
      <button
        className="btn btn-dark btn-block"
        onClick={() => {
          const eventObj = {
            eventName: 'Disaggregation',
            eventDate: new Date().toLocaleString(),
            owner: 'Distributor',
            data: 'location::Santa Clara',
          };
          props.storeEventDetails(eventObj);
          props.history.push('/newcaseDistributor')}}
      >
        <i className="fa fa-plus-square" /> Create a new case
      </button>
      <button
        className="btn btn-dark btn-block"
        onChange={event => {

          const eventObj = {
            eventName: 'Sent shipment',
            eventDate: new Date().toLocaleString(),
            owner: 'Distributor',
            data: 'location::Santa Clara',
          };
          props.storeEventDetails(eventObj);
          if (event.target.value === 'Pharmacy') {
            props.showRingBell({ hospitalRingBell: false });

            props.showRingBell({ pharmaRingBell: true });
            props.history.push('/shipcasePharmacy');

          }

          if (event.target.value === 'Hospital') {
            props.showRingBell({ pharmaRingBell: false });

            props.showRingBell({ hospitalRingBell: true });
            props.history.push('/shipcase');
          }


        }}
      >
        <i className="fa fa-ambulance" />Ship the Case
        <select className="form-control">
          <option>Select</option>
          <option>Pharmacy</option>
          <option>Hospital</option>
        </select>
      </button>
    </div>
    <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2">
      <img alt="image2" className="image1" src={distributor} />
      <p className="para2">Distributor</p>
    </div>
  </div>
}

export default Distributor;