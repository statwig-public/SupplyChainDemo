import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AreaChart } from 'react-easy-chart';
import * as services from '../../blockchain/library/pharmalib';
import { storeTransaction } from '../actions';
import RTChart from 'react-rt-chart';

class VerifyCase extends Component {

  componentWillMount() {
    this.setState({ caseID: this.getCurrentCaseID() });
    this.setState({fakeBottleList: [this.generateCaseIds(), this.generateCaseIds()]});
  }

  componentDidMount() {
    setInterval(() => this.forceUpdate(), 2000);
  }

  getCurrentCaseID() {
    const { transactions, multiChainTransactions} = this.props;
    const totalTransactions = [...transactions, ...multiChainTransactions];
    var len = totalTransactions.length;
    return len && totalTransactions[len - 1].caseId;
  }

  generateCaseIds() {
    return (
      Math.floor(100000 + Math.random() * 900000) +
      '-' +
      Math.floor(100000 + Math.random() * 900000) +
      '-' +
      Math.floor(1000 + Math.random() * 9000)
    );
  }

  async verifyCase(caller, callee) {
    const caseId = this.state.caseID;
    console.log('ID:' + caseId);
    await services.verifyCase(
      caseId,
      'distributor',
      'manufacturer',
      transactionHash => {
        this.setState({ transactionHash });
        const transactionObject = {
          transactionHash: transactionHash,
          bottleList: '',
          caseId,
        };
        this.props.storeTransaction(transactionObject);
        this.props.history.push('/');
      },
    );
  }

  async recordAlert(type,value,location) {
        const caseId = this.state.caseID;
        console.log('ID:' + caseId);
        await services.recordAlert(
            caseId,
            type,
            value,
            location,
            transactionHash => {
                this.setState({ transactionHash });
                const transactionObject = {
                    transactionHash: transactionHash,
                    bottleList: '',
                    caseId,
                };
                this.props.storeTransaction(transactionObject);
                this.props.history.push('/');
            },
        );
  }

  render() {
    const { transactions, ringBell, match, transactionsDistributor } = this.props;

    let bottleList = [];
    if(match.params.id === 'pharma') {
      bottleList =
        transactionsDistributor.length > 0 ? transactionsDistributor[0].bottleListPharma.split(',') : [];
    } else if(match.params.id === 'hospital') {
      bottleList =
        transactionsDistributor.length > 0 ? transactionsDistributor[1].bottleListHospital.split(',') : [];
    } else {
      bottleList =
        transactions.length > 0 ? transactions[0].bottleList.split(',') : [];
    }


    const data = ringBell.caseReturned ?
      [
          [
              { x: 'Sep24', y: 7.2 },
              { x: '09:00', y: 7.7 },
              { x: '14:00', y: 13 },
              { x: '19:00', y: 7.9 },
              { x: 'Sep25', y: 7.2 },
              { x: '05:00', y: 6.9 },
              { x: '10:00', y: 7.1 },
              { x: '15:00', y: 7.8 },
              { x: '20:00', y: 7.3 },
              { x: 'Sep26', y: 7.9 },
              { x: '06:00', y: 7.0 },
              { x: '11:00', y: 7.7 }
          ], [
          { x: 'Sep24', y: 18 },
          { x: '11:00', y: 18 }
      ], [
          { x: 'Sep24', y: 9 },
          { x: '11:00', y: 9 }
      ]
    ] : [
            [
                { x: 'Sep24', y: 7.2 },
                { x: '09:00', y: 7.7 },
                { x: '14:00', y: 7.6 },
                { x: '19:00', y: 7.9 },
                { x: 'Sep25', y: 7.2 },
                { x: '05:00', y: 6.9 },
                { x: '10:00', y: 7.1 },
                { x: '15:00', y: 7.8 },
                { x: '20:00', y: 7.3 },
                { x: 'Sep26', y: 7.9 },
                { x: '06:00', y: 7.0 },
                { x: '11:00', y: 7.7 }
            ], [
                { x: 'Sep24', y: 18 },
                { x: '11:00', y: 18 }
            ], [
                { x: 'Sep24', y: 9 },
                { x: '11:00', y: 9 }
            ]

    ];


    var chart = {
                axis: {
                    y: { min: 6, max: 18 }
                },
                point: {
                    show: true
                },
		size: {
		  height: 280,
            width: 880
		}


        };

      var rtdata = {
          date: new Date(),
          threshold: 18,
          limit: 10,
          temperature: this.props.ringBell.caseReturned ?
              //( ((Math.random() * 1) + 1) === 0 ? 8.1 : (((Math.random() * 1) + 1) === 1 ? 7.9 : 11) )
              (Math.floor((Math.random() * 2)) == 0 ? 7.9 : Math.floor((Math.random() * 2)) == 0 ? 8.1 : 12)  : ((Math.random() * 1.8) + 7)
      };
      if (rtdata.temperature > 10) {
          this.setState({raiseAlert:true});
      }

    return (
      <div className="container-fluid mt-3">
        <div className="row  mb-4">
          <div className="col-sm-6">
            <h2 className="d-flex justify-content-start">Verification</h2>
          </div>
          <div className="col-sm-6">
            <div className=" d-flex align-content-center justify-content-end">
              <button
                className="btn btn-dark mr-3"
                onClick={() => this.verifyCase('distributor', 'manufacturer')}
              >
                Submit
              </button>

              <button
                className="btn btn-dark"
                onClick={() => this.props.history.push('/')}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
        {this.props.ringBell.caseReturned && (
          <div className="row no-gutters justify-content-end">
            <div className="col-sm-12 text-center alert alert-danger p-1">
              Verification Failed !!
            </div>
          </div>
        )}
        <div className="row justify-content-center mb-3">
          {!this.props.ringBell.caseReturned  && <div className="col-sm-5">
            <h4 className="mb-3 text-dark">Product IDs from Scan</h4>
            <p className="text-dark">
              {transactions.length > 0 && transactions[0].caseId}
            </p>
            <ul className="list-none ml-3">
              {bottleList.map(bottle => (
                <li key={bottle} className="text-dark list-group-item">
                  {bottle}
                </li>
              ))}
            </ul>
          </div>}
          <div className="col-sm-5">
            <h4 className="text-info mb-3">
              Product IDs from Blockchain Record
            </h4>
            <p className="text-info">
              {transactions.length > 0 && transactions[0].caseId}
            </p>
            <ul className="list-none ml-3">
              {this.props.ringBell.caseReturned
                ? this.state.fakeBottleList.map(bottle => (
                    <li key={bottle} className="text-info list-group-item">
                      {bottle}
                    </li>
                  ))
                : bottleList.map(bottle => (
                    <li key={bottle} className="text-info list-group-item">
                      {bottle}
                    </li>
                  ))}
            </ul>
          </div>
        </div>

          {this.props.ringBell.caseReturned
            ? <h5 className="text-danger text-center">Found data mismatch</h5>
            : <h5 className="text-success text-center">All SGTINs from the scan match the record on Blockchain</h5>}
        <div className="row justify-content-center">
          <h4 className="col-12 text-center">
            Temperature Data for the shipment
          </h4>

              <AreaChart
                  yDomainRange={[0, 20]}
                  areaColors={['blue', '#F37506', '#18F306']}
                  title = "Temperature Analysis"
                  axisLabels={{x: 'Time', y: 'Temperature'}}
                  xType={'text'}
                  axes
                  grid
                  dataPoints
                  width={580}
                  height={280}
                  data={data}
              />

        </div>
<center>
          <div>
              <RTChart
	      chart={chart}
              fields={['threshold','limit','temperature']}
              data={rtdata} />
          </div>
    {this.state.raiseAlert == true  && <div><button  className="btn btn-dark mr-3" color="#FF000"
        onClick={() => this.recordAlert('sensor','temperature:'+rtdata.temperature,'San Jose')}> Record Temperature Alert
    </button> </div>}
</center>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    transactions: state.transactions,
    multiChainTransactions: state.multiChainTransactions,
    transactionsDistributor: state.transactionsDistributor,
    events: state.events,
    ringBell: state.ringBell

  };
}

export default connect(mapStateToProps, { storeTransaction})(VerifyCase);
