import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as services from '../../blockchain/library/pharmalib';

import { storeTransaction } from '../actions';

class NewCase extends Component {
  componentWillMount() {
    let caseIds = [];
    for (let i = 0; i < 15; i++) {
      caseIds.push({ active: false, id: this.generateCaseIds() });
    }
    this.setState({
      caseIds,
      caseId: "GTIN "+this.generateCaseIds(),
      selectedCaseIds: [],
    });
  }

  generateCaseIds() {
    return (
      444444 +
      '-' +
      Math.floor(100000 + Math.random() * 900000) +
      '-' +
      Math.floor(1000 + Math.random() * 9000)
    );
  }

  async makeCase(caller, location, bottleList) {
    const { caseId } = this.state;
    if (caller === 'manufacturer') {
      await services.makeManufacturerCase(
        caseId,
        location,
        bottleList,
        (transactionHash)=> {
          this.setState({ transactionHash });
          const transactionObject = {
            transactionHash,
            bottleList,
            caseId,
          };
          this.props.storeTransaction(transactionObject);
          this.props.history.push('/');
        }
      );
      /*var result = await mservices.makeCase(caseId,caller,location,bottleList);
        const transactionObject = {
            transactionHash: result.txnHash,
            bottleList,
            caseId,
        };
        this.props.storeTransaction(transactionObject);
        this.props.history.push('/');*/
    } else {
      await services.makeDistributorCase(
        this.state.pharmaID,
        location,
        bottleList,
      );
      await services.makeDistributorCase(
        this.state.hospID,
        location,
        bottleList,
      );
    }
  }

  onCaseIdSelectLeft(index) {
    let caseIds = this.state.caseIds;
    caseIds = JSON.parse(JSON.stringify(caseIds));
    caseIds[index].active = !caseIds[index].active;
    this.setState({ caseIds });
  }

  onCaseIdSelectRight(index) {
    let selectedCaseIds = this.state.selectedCaseIds;
    selectedCaseIds = JSON.parse(JSON.stringify(selectedCaseIds));
    selectedCaseIds[index].active = !selectedCaseIds[index].active;
    this.setState({ selectedCaseIds });
  }

  onSubmit = async () => {
    const bottleList = this.state.selectedCaseIds.reduce(
      (init, { id }, index) => {
        if (index === 0) return id;
        else return init + ',' + id;
      },
      '',
    );
    this.makeCase('manufacturer', 'India', bottleList);
  };

  onRightArrowClick = () => {
    const selectedCaseIds = this.state.caseIds.filter(caseId => caseId.active);
    const caseIds = this.state.caseIds.filter(caseId => !caseId.active);
    selectedCaseIds.forEach(caseId => (caseId.active = false));
    this.setState({
      selectedCaseIds: [...this.state.selectedCaseIds, ...selectedCaseIds],
      caseIds,
    });
  };

  onLeftArrowClick = () => {
    const caseIds = this.state.selectedCaseIds.filter(caseId => caseId.active);
    const selectedCaseIds = this.state.selectedCaseIds.filter(
      caseId => !caseId.active,
    );
    caseIds.forEach(caseId => (caseId.active = false));
    this.setState({
      caseIds: [...this.state.caseIds, ...caseIds],
      selectedCaseIds,
    });
  };
  render() {
    return (
      <div className="container-fluid mt-3">
        <div className="row">
          <div className="col-sm-6">
            <ul className="list-none">
              <li>
                <b>Product ID:</b> {this.state.caseId}
              </li>
              <li>
                <b>Batch Number: </b> BTST12345
              </li>
              <li>
                <b>Expiry Date: </b> 12-12-2022
              </li>
              <li>
                <b>Market Authorization Holder: </b> BTST Pharm
              </li>
            </ul>
          </div>
          <div className="col-sm-6">
            <div className=" d-flex align-content-center justify-content-center">
              <button
                className="btn btn-dark mr-3"
                onClick={this.onSubmit}
                disabled={this.state.selectedCaseIds.length === 0}
              >
                Submit
              </button>
              <button
                className="btn btn-dark"
                onClick={() => this.props.history.push('/')}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
        <div className="row mt-4 no-gutters">
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.caseIds.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectLeft(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
          <div className="col-sm-1 font30 text-center d-flex align-content-center justify-content-center">
            <i
              className="fa fa-arrow-circle-o-left"
              aria-hidden="true"
              onClick={this.onLeftArrowClick}
            />
            <i
              className="fa fa-arrow-circle-o-right"
              aria-hidden="true"
              onClick={this.onRightArrowClick}
            />
          </div>
          <div className="col-sm-5 grey_border">
            <ul className="list-none">
              {this.state.selectedCaseIds.map((item, index) => (
                <li
                  className={item.active ? 'active' : ''}
                  onClick={() => this.onCaseIdSelectRight(index)}
                  key={index}
                >
                  {item.id}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, { storeTransaction })(NewCase);
