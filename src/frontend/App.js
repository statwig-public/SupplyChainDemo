import React, { Component } from 'react';
import BellIcon from 'react-bell-icon';
import ModalPopup from './components/ModalPopup';
import './App.css';
import * as services from '../blockchain/library/pharmalib';
import { connect } from 'react-redux';
import { storeEventDetails, showRingBell } from '../frontend/actions';
import QRCode from 'qrcode.react';

/*import manufacture from './assets/images/manufacturer.png';
import distributor from './assets/images/distributor.png';
*/

const manufacture = "https://statwig.com/pharma/static/media/manufacturer.png";
const distributor ="https://statwig.com/pharma/static/media/distributor.png";
const pharmacyIcon ="https://statwig.com/pharma/static/media/pharmacy.png";
const hospitalIcon ="https://statwig.com/pharma/static/media/hospital.png";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      caseID: '',
      pharmaID: '',
      hospID: '',
      txnHash: '',
      eventName: '',
      eventDate: '',
      owner: '',
      data: '',
      openModal: false,
      txnInd: 0,

      transactions: [
        '0x90c71700ae959b3f60ad5c5b52f92631b86d1ee9156b67336cbd6362372d73a6',
        '0x90c71700ae959b3f60ad5c5b52f92631b86d1ee9156b67336cbd6362372d73a6',
      ],
    };
  }

  async makeCase(caller, location, bottleList) {
    var result = '';
    if (caller === 'manufacturer') {
      result = await services.makeManufacturerCase(
        this.state.caseID,
        location,
        bottleList,
      );
    } else {
      result = await services.makeDistributorCase(
        this.state.pharmaID,
        location,
        bottleList,
      );
      result = await services.makeDistributorCase(
        this.state.hospID,
        location,
        bottleList,
      );
    }
    this.setState({ txnHash: result.txnHash, eventDate: result.eventDate });
  }

  //Called by the owner of the case
  async shipCase(caller) {
    var result = '';
    if (caller === 'manufacturer') {
      result = await services.shipCase(
        this.state.caseID,
        caller,
        'distributor',
      );
    } else {
      result = await services.shipCase(this.state.pharmaID, caller, 'pharmacy');
      result = await services.shipCase(this.state.hospID, caller, 'hospital');
    }
    this.setState({ txnHash: result.txnHash, eventDate: result.eventDate });
  }

  //Called by the receiving participant
  async receiveCase(caller) {
    var result = '';
    if (caller === 'distributor') {
      result = await services.receiveCase(
        this.state.caseID,
        'manufacturer',
        caller,
        () => {},
      );
    } else if (caller === 'pharma') {
      result = await services.receiveCase(
        this.state.pharmaID,
        'distributor',
        'pharmacy',
        () => {},
      );
    } else {
      result = await services.receiveCase(
        this.state.hospID,
        'distributor',
        'hospital',
        () => {},
      );
    }
    this.setState({ txnHash: result.txnHash, eventDate: result.eventDate });
  }

  //Called by receiver of the case
  async verifyCase(caller) {
    var result = '';
    if (caller === 'distributor') {
      result = await services.verifyCase(
        this.state.caseID,
        caller,
        'manufacturer',
        () => {},
      );
    } else if (caller === 'pharma') {
      result = await services.verifyCase(
        this.state.pharmaID,
        'pharmacy',
        'distributor',
        () => {},
      );
    } else {
      result = await services.verifyCase(
        this.state.hospID,
        'hospital',
        'distributor',
        () => {},
      );
    }
    this.setState({ txnHash: result.txnHash, eventDate: result.eventDate });
  }

  openModal() {
    this.setState({ openModal: true });
  }

  closeModal() {
    this.setState({ openModal: false });
  }

  checkIfShipped() {
    var lenM = this.props.transactions.length;
    if (lenM > 0) {
      return this.props.transactions[lenM - 1].ringBell;
    } else {
      return false;
    }
  }
  render() {
    const { transactions, transactionsDistributor } = this.props;
    let redStyle = '';
    if (transactionsDistributor.length === 0) {
      redStyle = 'redStyle';
    }
    return (
      <div className="container-fluid mt-3">
        <div className="row">
          <div className="col-md-7 col-sm-12 pr-0">
            <div className="row ml-0 mr-0 grey_border">
              <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2">
                <button
                  className="btn btn-dark mb-2 btn-block"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Aggregation',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Manufacturer',
                      data: 'location::San Jose',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/newcase')
                  }

                  }
                >
                  <i className="fa fa-plus-square" />
                  Create a new case
                </button>

                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Sent shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Manufacturer',
                      data: 'destination:: Santa Clara,batch no:#BTST12345',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.showRingBell({ distributorRingBell: true });
                    this.props.history.push('/shipcase');
                  }}
                >
                  <i className="fa fa-ambulance" />Ship the Case
                </button>
              </div>
              <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2">
                <img alt="image1" className="image1" src={manufacture} />
                <p className="para1">Manufacturer</p>
              </div>
            </div>
            <div className="row ml-0 mr-0 grey_border">
              <div className="col-sm-6  pl-5 pr-5 pt-3 pb-2">
                <button
                  className="btn btn-dark mb-2 btn-block"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Receive shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Distributor',
                      data: 'location::Santa Clara',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/receiveCase');
                    this.props.showRingBell({ distributorRingBell: false });

                  }}
                >
                  <i className="fa fa-cart-arrow-down" /> Receive new case
                  {this.props.ringBell.distributorRingBell && (
                    <div className="bell_position">
                      <BellIcon
                        width="40"
                        active={true}
                        animate={true}
                        color="darkorange"
                      />
                    </div>
                  )}
                </button>
                <button
                  className="btn btn-dark mb-2 btn-block position-relative"
                  onClick={() => {
                      if (this.props.ringBell.caseReturned) {
                          const eventObj = {
                              eventName: 'Verification failed',
                              eventDate: new Date().toLocaleString(),
                              owner: 'Hospital',
                              data: 'confirmedBy::Distributor,expiryDate::21/09/2018',
                          };
                          this.props.storeEventDetails(eventObj);
                      } else {
                          const eventObj = {
                              eventName: 'Verified shipment',
                              eventDate: new Date().toLocaleString(),
                              owner: 'Distributor',
                              data: 'approvedBy::Manufacturer',
                          };

                          this.props.storeEventDetails(eventObj);
                      }
                    this.props.history.push('/verifyCase/distributor');

                  }}
                >
                  <i className="fa fa-handshake-o">Verification</i>
                </button>
                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Disaggregation',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Distributor',
                      data: 'location::Santa Clara',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/newcaseDistributor')}}
                >
                  <i className="fa fa-plus-square" /> Create a new case
                </button>


                <button
                  className="btn btn-dark btn-block"
                  onChange={event => {
                    if (event.target.value === 'Pharmacy') {
                      this.setState({
                        shipToPharmacy: true,
                        shipToHospital: false,
                      });
                      this.props.showRingBell({ hospitalRingBell: false });

                      this.props.showRingBell({ pharmaRingBell: true });
                    }

                    if (event.target.value === 'Hospital') {
                      this.setState({
                        shipToHospital: true,
                        shipToPharmacy: false,
                      });
                      this.props.showRingBell({ pharmaRingBell: false });

                      this.props.showRingBell({ hospitalRingBell: true });
                    }
                    const eventObj = {
                      eventName: 'Sent shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Distributor',
                      data: 'location::Santa Clara',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/shipcase');
                  }}
                >
                  <i className="fa fa-ambulance" />Ship the Case
                  <select className="form-control">
                    <option>Select</option>
                    <option>Pharmacy</option>
                    <option>Hospital</option>
                  </select>
                </button>
              </div>
              <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2">
                <img alt="image2" className="image1" src={distributor} />
                <p className="para2">Distributor</p>
              </div>
            </div>
            <div className="row ml-0 mr-0 custom_block">
              <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2 grey_border custom_receive">
                {this.props.ringBell.pharmaRingBell && (
                  <div className="bell_position">
                    <BellIcon
                      width="40"
                      active={true}
                      animate={true}
                      color="darkorange"
                    />
                  </div>
                )}
                <button
                  className="btn btn-dark mb-2 btn-block"
                  onClick={() => {
                    this.props.showRingBell({ pharmaRingBell: false });
                    this.receiveCase('pharma');
                    const eventObj = {
                      eventName: 'Receive Shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Manufacturer',
                      data: 'location::Hillsdale',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/receiveCasePharma');

                  }}
                >
                  <i className="fa fa-cart-arrow-down" /> Receive new case
                </button>
                <button
                  className="btn btn-dark btn-block position-relative"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Verified shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Pharmacy',
                      data: 'approvedBy::Distributor',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/verifyCase/pharma');

                  }}
                >
                  <i className="fa fa-file-text-o">Verification</i>
                </button>
                <img alt="image1" className="image3" src={pharmacyIcon} />
                <h4 className="d-flex align-items-end  justify-content-end">
                  Pharmacy
                </h4>
              </div>
              <div className="col-sm-6  pl-5 pr-5 pt-3 pb-2 grey_border">
                {this.props.ringBell.hospitalRingBell && (
                  <div className="bell_position">
                    <BellIcon
                      width="40"
                      active={true}
                      animate={true}
                      color="darkorange"
                    />
                  </div>
                )}
                <button
                  className="btn btn-dark mb-2 btn-block"
                  onClick={() => {
                    this.props.showRingBell({ pharmaRingBell: false });
                    const eventObj = {
                      eventName: 'Received shipment',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Hospital',
                      data: 'approvedBy::Distributor',
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.history.push('/receiveCaseHospital');

                  }}
                >
                  <i className="fa fa-cart-arrow-down" /> Receive new case
                </button>
                <button
                  className="btn btn-dark btn-block position-relative"
                  onClick={() => {

                        const eventObj = {
                            eventName: 'Verified shipment',
                            eventDate: new Date().toLocaleString(),
                            owner: 'Hospital',
                            data: 'approvedBy::Distributor',
                        };
                        this.props.storeEventDetails(eventObj);

                    this.props.history.push('/verifyCase/hospital');
                  }}
                >
                  <i className="fa fa-file-text-o">Verification</i>
                </button>
                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    const eventObj = {
                      eventName: 'Return medicines',
                      eventDate: new Date().toLocaleString(),
                      owner: 'Hospital',
                      data: 'sentTo::Distributor,reason::excess',
                      caseReturned: true
                    };
                    this.props.storeEventDetails(eventObj);
                    this.props.showRingBell({distributorRingBell: true});
                    this.props.history.push('/returnCase');
                  }}
                >
                  <i className="fa fa-file-text-o">Return</i>
                </button>
                <img alt="image1" className="image4" src={hospitalIcon} />
                <h4 className="d-flex align-items-end  justify-content-end">
                  Hospital
                </h4>
              </div>
            </div>
          </div>
          <div className="col-md-4 col-sm-12 pr-0 transaction_block">
            <div className="row ml-0 mr-0">
              <div className="transaction_history pt-2 pb-2 col-12 grey_border">
                <h4>Transaction History</h4>
                <ul className="list-none">
                  {transactions.map((item, index) => (
                    <li
                      key={item.transactionHash}
                      className={
                        index === transactions.length - 1 ? redStyle : ''
                      }
                    >
                        <a onClick={()=>this.setState({txnInd:index})}>{item.transactionHash}</a>
                    </li>
                  ))}
                  {transactionsDistributor.map((item, index) => (
                    <li
                      key={item.transactionHash}
                      className={
                        index === transactionsDistributor.length - 1
                          ? 'redStyle'
                          : ''
                      }
                    >
                        {item.transactionHash}
                    </li>
                  ))}
                </ul>
              </div>
              <div className="transaction_details pt-2 pb-2 col-12 grey_border">
                <h4>Transaction Details</h4>

                <ul className="list-none">
                  {this.props.events.length > 0 && <li>Event name: {this.props.events[this.state.txnInd].eventName}</li>}
                  {this.props.events.length > 0 && <li>Event date: {this.props.events[this.state.txnInd].eventDate}</li>}
                  {this.props.events.length > 0 && <li>Owner: {this.props.events[this.state.txnInd].owner}</li>}
                  {this.props.events.length > 0 && <li>Additional Data: {this.props.events[this.state.txnInd].data}</li>}
                </ul>
              </div>

              <div className="upload_certificate pt-2 pb-2 col-12 grey_border">
                <h4>Certification and Duplication</h4>
                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    this.props.history.push('/certificate');
                  }}
                >
                  <i className="fa fa-cloud-upload" />   Upload Certificate
                </button>

                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    this.props.history.push('/checkipfsduplicacy');
                  }}
                >
                  <i className="fa fa-cloud-upload" />   Check IPFS Duplicacy
                </button>

                <button
                  className="btn btn-dark btn-block"
                  onClick={() => {
                    this.props.history.push('/checklocationduplicacy');
                  }}
                >
                  <i className="fa fa-cloud-upload" />   Check Location Duplicacy
                </button>


              </div>

              <div className="journey pt-2 pb-2 col-12 grey_border">
                <h4>Product Journey</h4>
                  <QRCode value={'https://statwig.com/journey/'} /> 
              </div>


            </div>
          </div>
        </div>
        <ModalPopup
          open={this.state.openModal}
          title="Package Information"
          handleClose={() => this.closeModal()}
        />
        {this.state.openModal && <div className="modal-backdrop fade show" />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    transactions: state.transactions,
    transactionsDistributor: state.transactionsDistributor,
    events: state.events,
    ringBell: state.ringBell,
  };
}

export default connect(mapStateToProps, { storeEventDetails, showRingBell })(
  App,
);
