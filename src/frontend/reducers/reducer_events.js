import { STORE_EVENT_DETAILS } from '../actions';

const INITIAL_STATE = [];

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
      case STORE_EVENT_DETAILS:
            return [...state, action.payload];
        default:
            return state;
    }
}
