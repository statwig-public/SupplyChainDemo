import { combineReducers } from 'redux';
import TransactionsReducer from './reducer_transactions';
import TransactionsMCReducer from './reducer_mc_transactions';
import DistributorReducer from './reducer_distributor';
import CaseIDReducer from './reducer_caseId';
import EventsReducer from './reducer_events';
import RingReducer from './reducer_bell';

const rootReducer = combineReducers({
  transactions: TransactionsReducer,
  multiChainTransactions: TransactionsMCReducer,
  transactionsDistributor: DistributorReducer,
  events: EventsReducer,
  caseId: CaseIDReducer,
  ringBell: RingReducer
});

export default rootReducer;
