import { STORE_DISTRIBUTOR_TRANSACTIONS } from '../actions';

const INITIAL_STATE = [];

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case STORE_DISTRIBUTOR_TRANSACTIONS:
      return [...state, action.payload];
    default:
      return state;
  }
}
