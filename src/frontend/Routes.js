import React, { Component } from 'react';
import App from './App';
import {
    BrowserRouter as Router,
  Route,
    Switch
} from "react-router-dom";
import NewCase from './components/NewCase';
import ShipCase from './components/ShipCase';
import ReceiveCase from './components/ReceiveCase';
import DistributorNewCase from "./components/DistributorNewCase";
import ReturnCase from "./components/ReturnCase";
import VerifyCase from "./components/VerifyCase";
import ReceiveCasePharma from "./components/ReceiveCasePharma";
import ReceiveCaseHospital from "./components/ReceiveCaseHospital";
import Certificate from "./components/certificate";
import IpfsDuplicacy from "./components/ipfsduplicacy";
import LocationDuplicacy from "./components/locationduplicacy";


class Routes extends Component {
  render() {
    return (
      <Router>
          <Switch>
            <Route exact path="/" component={App} />
            <Route path="/newcase" component={NewCase} />
            <Route path="/shipcase" component={ShipCase} />
            <Route path="/receiveCase" component={ReceiveCase} />
            <Route path="/receiveCasePharma" component={ReceiveCasePharma} />
            <Route path="/receiveCaseHospital" component={ReceiveCaseHospital} />
            <Route path="/returncase" component={ReturnCase} />
            <Route path="/verifyCase/:id" component={VerifyCase} />
            <Route path="/newcaseDistributor" component={DistributorNewCase} />
            <Route path="/certificate" component={Certificate} />
            <Route path="/checkipfsduplicacy" component={IpfsDuplicacy} />
            <Route path="/checklocationduplicacy" component={LocationDuplicacy} />
          </Switch>
      </Router>
    );
  }
}

export default Routes;
