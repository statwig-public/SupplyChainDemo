import web3 from './web3';
import Bottle from './build/Bottle.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(Bottle.interface),'0x16322C79E478f14b608A19360A22d567582ddbDb'
    )
};
