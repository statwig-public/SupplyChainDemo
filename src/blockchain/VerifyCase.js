import web3 from './web3';
import VerifyCase from './build/VerifyCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(VerifyCase.interface),'0x7e37A11f91a4ea117945F3c0EBC621314623501b'
    )
};
