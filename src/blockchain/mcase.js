import web3 from './web3';
import MCase from './build/MCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(MCase.interface),'0x15bc377911260992f44D8f3c38830FCc1f33e8a5'
    )
};
