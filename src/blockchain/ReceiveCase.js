import web3 from './web3';
import ReceiveCase from './build/ReceiveCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(ReceiveCase.interface),'0xbd7A1f179fD5cF4496f79f45033E6A74dB11021A'
    )
};
