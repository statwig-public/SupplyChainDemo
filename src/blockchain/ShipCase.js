import web3 from './web3';
import ShipCase from './build/ShipCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(ShipCase.interface),'0x0a37396fe2b11b4de3fCB37631cc4182B178e778'
    )
};
