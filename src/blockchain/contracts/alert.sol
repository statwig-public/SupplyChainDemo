pragma solidity ^0.4.24;

contract Alert {

    struct alert {
       string ID; //caseID on which the sensed parameter crossed the threshold
       string alertType; //'sensor' is a type of alert when the sensed value exceeds a pre-defined threshold
       string alertDate; //takes both date and time in Zulu format
       string alertValue; //this is in the form 'paramName:paramVal'
       string location; //location at which this is manufactured
    }

    alert[] public alertList;
    mapping(string => bool) alertIDs;

    function createAlert (string _ID, string _alertType, string _alertDate, string _alertValue, string _location) public view returns (uint){

       require(!alertIDs[_ID]);

       alert memory newAlert = alert({ID: _ID,
                                    alertType: _alertType,
                                    alertDate: _alertDate,
                                    alertValue: _alertValue,
                                    location: _location});
       alertIDs[_ID] = true;
       alertList.push(newAlert);


    }

    function getAlertCount() public view returns(uint) {
               return alertList.length;
    }
}
