pragma solidity ^0.4.24;

contract DCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct dcase {
       string ID;
       string createDate;
       string srcLocation;
       string bList; //can hold a maximum of 12
    }

    dcase[] public dcaseList;
    mapping(string => bool) dcaseIDs;

    function createDCase (string _ID, string _createDate, string _srcLocation, string _bList) public {

       require(!dcaseIDs[_ID]);

       dcase memory newDCase = dcase({ID: _ID,
                                      createDate: _createDate,
                                      bList: _bList,
                                      srcLocation: _srcLocation
                                      });

       dcaseIDs[_ID] = true;
       dcaseList.push(newDCase);
    }

    function getDCaseCount() public view returns(uint) {
           return dcaseList.length;
    }
}
