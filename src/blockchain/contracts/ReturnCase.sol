pragma solidity ^0.4.24;

contract ReturnCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct medCase {
       string ID;
       string bList; //not necessarily those bottles which got into the case
       string returnBy; //person who ships the cases
       string returnTo;
       string returnDate;
    }

    medCase[] public returnList;
    mapping(string => bool) returnedCaseIDs;

    function returnCase (string _ID, string _bList, string _returnBy, string _returnTo, string _returnDate) public {

       require(!returnedCaseIDs[_ID]);

       medCase memory newCase = medCase({ID: _ID,
                                    bList: _bList,
                                    returnBy: _returnBy,
                                    returnTo: _returnTo,
                                    returnDate: _returnDate
                                    });

       returnedCaseIDs[_ID] = true;
       returnList.push(newCase);
    }

    function getReturnedCaseCount() public view returns(uint) {
       return returnList.length;
    }
}
