pragma solidity ^0.4.24;

contract Bottletry {

    //A single bottle is not shipped; only the cases are shipped
    struct bottle {
       string ID;
       string createDate; //takes both date and time in Zulu format
       string expiryDate;
       string location; //location at which this is manufactured
    }

    bottle[] public bottleList;
    mapping(string => bool) bottleIDs;

    function createBottle (string _ID, string _createDate, string _expiryDate, string _location) public view returns (uint){

       require(!bottleIDs[_ID]);

       bottle memory newBottle = bottle({ID: _ID,
                                    createDate: _createDate,
                                    expiryDate: _expiryDate,
                                    location: _location});
       bottleIDs[_ID] = true;
       bottleList.push(newBottle);


    }

    function getBottleCount() public view returns(uint) {
               return bottleList.length;
    }
}
