pragma solidity ^0.4.24;

contract VerifyCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct medCase {
       string ID;
       string requestBy; //person who ships the cases
       string requestTo;
       string verifiedDate;
    }

    medCase[] public verifiedList;
    mapping(string => bool) verifiedCaseIDs;

    function verifyCase (string _ID, string _requestBy, string _requestTo, string _verifiedDate) public {

       require(!verifiedCaseIDs[_ID]);

       medCase memory newCase = medCase({ID: _ID,
                                    requestBy: _requestBy,
                                    requestTo: _requestTo,
                                    verifiedDate: _verifiedDate
                                    });

       verifiedCaseIDs[_ID] = true;
       verifiedList.push(newCase);
    }

    function getVerifiedCaseCount() public view returns(uint) {
       return verifiedList.length;
    }
}
