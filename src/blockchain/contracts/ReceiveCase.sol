pragma solidity ^0.4.24;

contract ReceiveCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct medCase {
       string ID;
       string sentBy;
       string receivedBy; //person who ships the cases
       string receivedDate;
    }

    medCase[] public receivedList;
    mapping(string => bool) receivedCaseIDs;

    function receiveCase (string _ID, string _sentBy, string _receivedBy, string _receivedDate) public {

       require(!receivedCaseIDs[_ID]);

       medCase memory newCase = medCase({ID: _ID,
                                    sentBy: _sentBy,
                                    receivedBy: _receivedBy,
                                    receivedDate: _receivedDate
                                    });

       receivedCaseIDs[_ID] = true;
       receivedList.push(newCase);
    }

    function getReceivedCaseCount() public view returns(uint) {
       return receivedList.length;
    }
}
