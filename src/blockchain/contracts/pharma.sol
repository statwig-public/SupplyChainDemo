pragma solidity ^0.4.24;

contract pharma {

    //A single bottle is not shipped; only the cases are shipped
    struct bottle {
       string ID;
       string createDate; //takes both date and time in Zulu format
       string expiryDate;
       string location; //location at which this is manufactured
    }

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct mcase {
       string ID;
       string createDate;
       string srcLocation;
       string destLocation; //if it is non-empty, it means that the case has been shipped
       string shippedDate;
       string bList; //can hold a maximum of 12
    }

    //A case made by the distributor can contain any number of bottles
    struct dcase {
       string ID;
       string createDate;
       string bList; //list of bottle IDs to be packed in the case
       string srcLocation;
       string destLocation; //if it is non-empty, it means that the case has been shipped
       string shippedDate;
    }

    bottle[] public bottleList;
    mcase[] public mcaseList;
    dcase[] public dcaseList;
    mapping(string => bool) bottleIDs;
    mapping(string => bottle) bottleMap;
    mapping(string => bool) mcaseIDs;
    mapping(string => bool) dcaseIDs;


    function createBottle (string _ID, string _createDate, string _expiryDate, string _location) public {

       require(!bottleIDs[_ID]);
       /*
       bottle memory newBottle = bottle({ID: _ID,
                                    createDate: _createDate,
                                    expiryDate: _expiryDate,
                                    location: _location});
       bottleIDs[_ID] = true;
       bottleList.push(newBottle);
       */
       bottle memory newBottle = bottleMap[_ID];
       newBottle.ID = _ID;
       newBottle.createDate = _createDate;
       newBottle.expiryDate = _expiryDate;
       newBottle.location = _location;
    }

    function createMCase (string _ID, string _createDate, string _srcLocation, string _bList) public {

       require(!mcaseIDs[_ID]);

       mcase memory newMCase = mcase({ID: _ID,
                                      createDate: _createDate,
                                      bList: _bList,
                                      srcLocation: _srcLocation,
                                      destLocation: "",
                                      shippedDate: ""});

       mcaseIDs[_ID] = true;
       mcaseList.push(newMCase);
    }

    function createDCase (string _ID, string _createDate, string _srcLocation, string _bList) public {

           require(!dcaseIDs[_ID]);
           dcase memory newDCase = dcase({ID: _ID,
                                          createDate: _createDate,
                                          bList: _bList,
                                          srcLocation: _srcLocation,
                                          destLocation: "",
                                          shippedDate: ""});
           dcaseIDs[_ID] = true;
           dcaseList.push(newDCase);
    }


    function fetchBottle(string ID) public view returns(string,string,string,string) {
           //check whether the bottle is present or not
           require(bottleIDs[ID]);
           bottle memory b = bottleMap[ID];
           return (b.ID, b.createDate, b.expiryDate, b.location);
    }

    function getBottleCount() public view returns(uint) {
           return bottleList.length;
    }

    function getMCaseCount() public view returns(uint) {
           return mcaseList.length;
    }

    function getDCaseCount() public view returns(uint) {
           return dcaseList.length;
    }

}
