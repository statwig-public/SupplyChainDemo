import web3 from './web3';
import DCase from './build/DCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(DCase.interface),'0x08BE6d43454988b198913562569174770fFE3F80'
    )
};
